%---------------------------
\listfiles
\documentclass[(((lang))),BLN,(((calibration_type)))]{kalibrierschein}

\object{(((ObjectOfCalibration)))}
\manufacturer{(((Producer)))}
\type{(((Type)))}
\serialNo{(((Serial)))}
\applicant{%
  {(((CustomerName)))}%
  {(((CustomerStreet)))}%
  {(((CustomerZipcode))) (((CustomerTown)))}%
  {(((CustomerLand)))}%
}
\refNo{(((ReferenceNo)))}
\calibMark{% 
((* if calibration_type == 'IK' *))%
  (((Certificate)))~IK~(((ShortYear)))%
((*else*))%
  (((Certificate)))~PTB~(((ShortYear)))%
((*endif*))% 
}

\calibDate{(((MeasurementDate)))}
\byOrder{Dr.\,Matthias Bernien}
\examiner{(((Examiner)))}
\certificateDate{(((CertificateDate)))} 

% TODO: Create via XML processing!
\resultTable[ calibration gas: nitrogen ]{ 7 }{%
\multicolumn{1}{c}{{\(p_\text{cal}\)}}&\multicolumn{1}{c}{{\(p_\text{ind} - p_\text{offs}\)}}&\multicolumn{1}{c}{{\(e\)}}&\multicolumn{1}{c}{{\(CF\)}}&\multicolumn{1}{c}{{\(U(e)\)}}&\multicolumn{1}{c}{{\(U(CF)\)}}&\multicolumn{1}{c}{range}\\
\multicolumn{1}{c}{{(\(\si{\pascal}\))}}&\multicolumn{1}{c}{{(\(\si{\pascal}\))}}&\multicolumn{1}{c}{}&\multicolumn{1}{c}{}&\multicolumn{1}{c}{}&\multicolumn{1}{c}{}&\multicolumn{1}{c}{}\\\toprule\endhead
\num{9.9820e-02}&\num{1.0360e-01}&\num{0.0379}&\num{0.9635}&\num{0.0029}&\num{0.0026}&\num{X0.01}\\
\num{2.0145e-01}&\num{2.0856e-01}&\num{0.0353}&\num{0.9659}&\num{0.0028}&\num{0.0026}&\num{X0.01}\\
\num{3.9933e-01}&\num{4.1238e-01}&\num{0.0327}&\num{0.9684}&\num{0.0026}&\num{0.0024}&\num{X0.01}\\
\num{5.9719e-01}&\num{6.1581e-01}&\num{0.0312}&\num{0.9698}&\num{0.0026}&\num{0.0024}&\num{X0.01}\\
\num{8.0093e-01}&\num{8.2473e-01}&\num{0.0297}&\num{0.9711}&\num{0.0025}&\num{0.0024}&\num{X0.01}\\
\num{9.9852e-01}&\num{1.0270e+00}&\num{0.0286}&\num{0.9722}&\num{0.0025}&\num{0.0023}&\num{X0.01}\\
\num{2.0065e+00}&\num{2.0569e+00}&\num{0.0251}&\num{0.9755}&\num{0.0025}&\num{0.0023}&\num{X0.1}\\
\num{3.9890e+00}&\num{4.0613e+00}&\num{0.0181}&\num{0.9822}&\num{0.0024}&\num{0.0023}&\num{X0.1}\\
\num{6.0071e+00}&\num{6.0883e+00}&\num{0.0135}&\num{0.9867}&\num{0.0024}&\num{0.0023}&\num{X0.1}\\
\num{7.9824e+00}&\num{8.0665e+00}&\num{0.0105}&\num{0.9896}&\num{0.0024}&\num{0.0023}&\num{X0.1}\\
\num{9.9882e+00}&\num{1.00700e+01}&\num{0.0082}&\num{0.9919}&\num{0.0016}&\num{0.0016}&\num{X0.1}\\
\num{1.9961e+01}&\num{2.0012e+01}&\num{0.0026}&\num{0.9974}&\num{0.0016}&\num{0.0016}&\num{X1}\\
\num{3.9931e+01}&\num{3.9919e+01}&\num{-0.0003}&\num{1.0003}&\num{0.0016}&\num{0.0016}&\num{X1}\\
\num{5.9890e+01}&\num{5.9842e+01}&\num{-0.0008}&\num{1.0008}&\num{0.0016}&\num{0.0016}&\num{X1}\\
\num{7.9855e+01}&\num{7.9788e+01}&\num{-0.0008}&\num{1.0008}&\num{0.0016}&\num{0.0016}&\num{X1}\\
\num{9.9809e+01}&\num{9.9756e+01}&\num{-0.0005}&\num{1.0005}&\num{0.0016}&\num{0.0016}&\num{X1}\\
\num{1.29523e+02}&\num{1.2969e+02}&\num{0.0013}&\num{0.9987}&\num{0.0016}&\num{0.0016}&\num{X1}\\
}

 
\begin{document}

\printFirstPage

\section{ Description relating to calibration device }
 
  The device was shipped under vacuum pressure equipped with a valve.
  
  It was read out via the GPIB interface. 
  

\section{ Calibration procedure }
 
  The head was mounted in a horizontal orientation (vertical orientation of the 
  membrane). Calibrations were performed in an uprising sequence in the 
  pressure range \(\SI{0.1}{Pa}\) to \(\SI{130}{Pa}\). The calibration pressure 
  was established in the primary standard SE3 metrologically linked to the 
  primary standard SE2 of PTB applying the static expansion method. The gas 
  temperature during the calibration with nitrogen was 
  \SI{295.939+-0.049}{\kelvin} and the room temperature  
  \SI{295.839+-0.091}{\kelvin}.
  
  The device was operated with the following setup:
  \begin{itemize}[leftmargin=1cm]
    \item[\textbf{\texttt{Heater}:}] \texttt{on}          
    \item[\textbf{\texttt{ZeroEnabbed}:}] \texttt{on}          
    \item[\textbf{\texttt{Unit}:}] \texttt{mbar}          
    \item[\textbf{\texttt{Averaging}:}] \texttt{20}          
    \item[\textbf{\texttt{Compensation}:}] \texttt{+-500}          
    \item[\textbf{\texttt{SensorRange}:}] \texttt{1.33322}          
    \item[\textbf{\texttt{Response}:}] \texttt{400ms}          
    \item[\textbf{\texttt{TypeHead}:}] \texttt{1Torr}      
  \end{itemize}
  
  Offset samples were measured before the calibration at a base pressure below 
  \(\SI{1E-6}{\pascal}\) to determine the zero stability. Standard 
  uncertainties of \SI{2.7E-5}{\pascal} (X0.01), \SI{5.1E-5}{\pascal} (X0.1) 
  and \SI{7.2E-4}{\pascal} (X1) were estimated for the zero stability of the 
  device.  For each generated calibration pressure, the mean of 10 readings of 
  the customer gauge was taken. Before each calibration point the offset 
  \(p_\text{offs}\) (10 readings) was checked at the base pressure and 
  substracted from the subsequent indication \(p_\text{ind}\) to give the 
  corrected indicated value \(p_\text{corr}\).
  

\section{ Relative error of pressure indication and correction factor }
 
  The relative error \(e\) of the corrected indicated pressure 
  \(p_\text{corr}\)  (with \(p_\text{corr} = p_\text{ind} - p_\text{offs}\)) at 
  the time of calibration is defined as:\[e = \frac{p_\text{ind} - 
  p_\text{offs}}{p_\text{cal}} - 1\] where \(p_\text{cal}\) denotes the 
  calibration pressure as generated in the primary standard. From this, the 
  real pressure \(p\) can be calculated from the indicated and offset pressure 
  by:\[p = \frac{p_\text{ind} - p_\text{offs}}{e + 1}\] 
  
  The correction factor \(CF\) is defined by: 
  \[CF =\frac{p_\text{cal}}{p_\text{ind} - p_\text{offs}}\] 
  and can be used to calculate the real pressure \(p\) by: 
  \[p = CF (p_\text{ind} - p_\text{offs})\]
  

\section{ Result of the calibration }
 
  The results of the measurements are given below. The last columns of the 
  table provide the uncertainty \(U\) of \(e\) and \(U\) of \(CF\) at the time 
  of calibration. Included is the repeatability of the measurement under 
  otherwise identical conditions (\(p_\text{cal}\), \(T\)).
  
  \printResultTable
  

\section{ Temperature correction }
 
  In the molecular flow regime (ideal gas independent pressure difference due 
  to thermal transpiration) the relative deviation \(e\) depends on the 
  temperature difference between the thermostated head and the gas in the 
  calibration chamber. In the transition regime between molecular flow and 
  viscous flow (gas independent pressure equilibrium), which is roughly between 
  \(\SI{0.1}{\pascal}\) and \(\SI{100}{\pascal}\), the deviation is temperature 
  and gas-species dependent. If during use the gas temperature is significantly 
  different  (\(\SI{1}{\kelvin}\)) from the temperature during calibration, an 
  additional correction must be applied in the transition and molecular flow 
  regime and used for the calculation of \(p\):  \[e' = e_\text{vis} + (e - 
  e_\text{vis})  \frac{ \sqrt{T_2/{T_1\!}'}-1 }{ \sqrt{T_2/T_1}-1} \]  If the 
  correction factor \(CF\) is used:   \[CF' = CF_\text{vis} + (CF - 
  CF_\text{vis})  \frac{ \sqrt{T_2/{T_1\!}'}-1 }{ \sqrt{T_2/T_1}-1} \]   where 
  \(T_1\) (in \si{\kelvin}) is the gas temperature during calibration, 
  \({T_1\!}'\) (in \si{\kelvin}) the temperature of the gas at use, \(T_2\) (in 
  \si{\kelvin}) the temperature of the thermostated head (about 
  \(\SI{318}{\kelvin}\)), \(e_\text{vis}\) and \(CF_\text{vis}\)  the average 
  indication error and  the average correction factor in the viscous flow 
  regime (\(>\SI{100}{\pascal}\)), respectively. \(e\) and \(CF\) are the error 
  and the correction factor in the molecular flow and transition regime 
  (\(<\SI{100}{\pascal}\)), respectively. The value of \(e_\text{vis}\) is 
  estimated at \(\SI{-0.0005}{}\). The value of \(CF_\text{vis}\) is estimated 
  at \(\SI{1.0005}{}\).  
  

\section{ Uncertainty }
 
  The uncertainty stated is the expanded measurement uncertainty obtained by 
  multiplying the standard measurement uncertainty by the coverage factor 
  \(k=2\). It has been determined in  accordance  with  the “Guide  to  the  
  Expression  of Uncertainty  in  Measurement (GUM)”. The  value  of  the  
  measurand  then normally  lies,  with  a probability of approximately 
  \SI{95}{\percent}, within the attributed coverage interval.
  

\printLastPage
\end{document}
%---------------------------
