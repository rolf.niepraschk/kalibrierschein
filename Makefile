
# Rolf Niepraschk, 2019/04/25, Rolf.Niepraschk@ptb.de

.SUFFIXES : .tex .ltx .dvi .ps .pdf .eps

MAIN = kalibrierschein-test

LATEX = lualatex 
#LATEX = xelatex 
#LATEX = pdflatex

PICS = Adler.pdf CIPM-MRA.png PTB-Logo.pdf

ADDINPUTS = ks-common.clo IK-2019.clo KK-2019.clo

all : $(MAIN).pdf

%.tex : %.xml 
	./sample_cal_creation.py templates/kalibrierschein-test.tex.tpl < $< > $@

%.pdf : %.tex $(ADDINPUTS) $(PICS)
	$(LATEX) $<
	$(LATEX) $<

veryclean : clean
	$(RM) $(MAIN).pdf $(MAIN).tex

clean :
	$(RM) $(addprefix $(MAIN), .log .aux .bbl .blg)

debug :
	@echo $(ADDINPUTS)

.PRECIOUS: $(MAIN).tex
