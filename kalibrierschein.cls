
\RequirePackage{iftex}

\iftutex\else% any 8-bit-TeX engine
  \@latex@error{works only with `LuaLaTeX' or `XeLaTeX'}{%
    type:\MessageBreak 
    lualatex \jobname.tex\MessageBreak
    or\MessageBreak 
    xelatex \jobname.tex
  }%
  \expandafter\@@end 
\fi

\setcounter{errorcontextlines}{100}
\RequirePackage{ifdraft}
\newcommand*\ks@location{Berlin}
\newcommand*\ks@kalType{KK}
\newcommand*\ks@layout{-2019}% derzeit nicht variabel
\newcommand*\ks@langs{ngerman,main=english}
\newif\ifnoCMC \noCMCfalse
\newif\iftestreport \testreportfalse
\NeedsTeXFormat{LaTeX2e}[1999/12/01]
\ProvidesClass{kalibrierschein}
  [2024/05/02 v0.7b documentclass PTB `Kalibrierschein' (RN/PTB)]
\DeclareOption{BS}{\def\ks@location{Braunschweig}}
\DeclareOption{BLN}{\def\ks@location{Berlin}}
\DeclareOption{KK}{\edef\ks@kalType{\CurrentOption}}
\DeclareOption{IK}{\edef\ks@kalType{\CurrentOption}}
%\DeclareOption{2015}{\edef\ks@layout{-\CurrentOption}}
\DeclareOption{german}{\def\ks@langs{english,main=ngerman}}
\DeclareOption{ngerman}{\def\ks@langs{english,main=ngerman}}
\DeclareOption{de}{\def\ks@langs{english,main=ngerman}}
\DeclareOption{english}{\def\ks@langs{ngerman,main=english}}
\DeclareOption{en}{\def\ks@langs{ngerman,main=english}}
\DeclareOption{noCMC}{\noCMCtrue}
\DeclareOption{testreport}{\testreporttrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax
\PassOptionsToPackage{paper=a4,pagesize,twoside=false,headinclude}
  {typearea}

%Setze die richtige Kennung je nach CMC / Test Report
\iftestreport
  \ifnoCMC
    \newcommand*\ks@doc@style@code{393 03A m}% Kennung der Word-Vorlage
  \else
    \newcommand*\ks@doc@style@code{393 03B o}% Kennung der Word-Vorlage
  \fi
\else
  \ifnoCMC
    \newcommand*\ks@doc@style@code{391 00A p}% Kennung der Word-Vorlage
  \else
    \newcommand*\ks@doc@style@code{391 00B q}% Kennung der Word-Vorlage
  \fi
\fi

\LoadClass[%
,fontsize=11pt
,headings=small
,numbers=enddot
,parskip=half
]{scrartcl}

\RequirePackage[%
headheight=0bp,
marginparwidth=0pt,
left=69bp,
textwidth=489bp,
textheight=669bp,
top=85bp,
headsep=0bp,
head=12.85pt,%???
footskip=0bp,
]{geometry}

\raggedbottom % ???

%\RequirePackage{showframe}% Zur Orientierung

\RequirePackage[\ks@langs]{babel}
\RequirePackage[autostyle=true,german=quotes,maxlevel=3]{csquotes}%
\RequirePackage{iflang}
\IfLanguageName{ngerman}{%
  \newcommand*\ks@draftName{Entwurf}%
  \defineshorthand{"`}{\openautoquote}
  \defineshorthand{"'}{\closeautoquote}
  %-------------------------------------------------------
  \babelprovide[hyphenrules=ngerman-x-latest]{ngerman}
  %-------------------------------------------------------
}{%
  \newcommand*\ks@draftName{Draft}%
  \useshorthands{"}
  \defineshorthand{"`}{\openautoquote}
  \defineshorthand{"'}{\closeautoquote}
}
\AtEndOfClass{\shorthandon{"}}
\MakeAutoQuote{»}{«}

% Anpassen der Schriftbefehle

% nach scrsize11pt.clo
\def\normalsize{%
  \@setfontsize\normalsize{11.2}{12.85}%
  \abovedisplayskip 11\p@ \@plus3\p@ \@minus6\p@
  \abovedisplayshortskip \z@ \@plus3\p@
  \belowdisplayshortskip 6.5\p@ \@plus3.5\p@ \@minus3\p@
  \belowdisplayskip \abovedisplayskip
  \let\@listi\@listI
}

\def\huge{\@setfontsize\huge{20.26}{25.3}}
\def\large{\@setfontsize\large{12.17}{14.2}}
\def\tiny{\@setfontsize\tiny\@viipt\@viiipt}
\let\ks@titelpagefont=\normalsize

\RequirePackage{textcomp}% ???

\RequirePackage{url,array,xltabular}

\PassOptionsToPackage{final}{graphicx}
\RequirePackage{eso-pic,graphicx,booktabs}
\RequirePackage{zref-totpages}
\RequirePackage{amsmath}

\input{ks-fontdef.clo}

% Listenabstände reduzieren
\RequirePackage[neverdecrease]{paralist}
\let\itemize\compactitem
\let\enditemize\endcompactitem
\let\enumerate\compactenum
\let\endenumerate\endcompactenum
\let\description\compactdesc
\let\enddescription\endcompactdesc
\pltopsep=\medskipamount
\plitemsep=\smallskipamount

\newcommand*\ks@PTB{Physikalisch-Technische Bundesanstalt}
\newcommand*\ks@BSuB{Braunschweig und Berlin}

\RequirePackage{picture}% TODO: noch nötig?

\newdimen\ks@tempdima
\newdimen\ks@tempdimb
\newdimen\ks@tempdimc
\newdimen\ks@tempdimd
\newdimen\ks@tempdime
\newdimen\ks@tempdimf

\newcommand*\ks@tempa{}
\newcommand*\ks@tempb{}
\newcommand*\ks@tempc{}
\newcommand*\ks@tempd{}
\newcommand*\ks@tempe{}

\newtoks\ks@tab
\newcommand*\ks@colSpec{}

\newcommand*\ks@add[1]{%
  \ks@tab\expandafter{\the\ks@tab#1}%
}

\newcommand*\ks@addTabLine[1]{%
  \def\@tempc{}%
  \@for\@tempa:=#1\do{%
    \begingroup
      \let\\=\relax\let\newline=\relax
      \edef\@tempb{\endgroup\noexpand\ks@tab{%
        \the\noexpand\ks@tab\@tempc\@tempa}}%
    \@tempb
    \def\@tempc{\noexpand&}% \noexpand hier unnötig, aber ...
  }%
  %\showthe\ks@tab
}
\RequirePackage{varwidth}
\newcolumntype{v}{>{\varwidth[t]{\linewidth}}l<{\endvarwidth}}
\newcommand*\ks@getColSpec[2]{% #1 = comma list, #2 = result macro
  \let#2=\@empty
  \@for\@tempa:=#1\do{%
    \edef#2{#2v}%
  }%
  \ifx#2\@empty
    \def#2{>{\let\\=\newline}v}%
  \fi
}

\newcommand*\ks@printDevicesTable[3]{% #1 = 1. offset, #2 = 2. offset
  \ks@tab{}%
  \ks@getColSpec{\ks@type}{\ks@colSpec}%
  \edef\@tempa{\noexpand\begin{tabular}[t]{@{}\ks@colSpec @{}}}%
  \ks@tab\expandafter{\@tempa}% 
  \ks@addTabLine{\ks@manufacturer}%
  \ks@add{\tabularnewline[#1]}%
  \ks@addTabLine{\ks@type}%
  \ks@add{\tabularnewline[#2]}%
  \ks@addTabLine{\ks@serialNo}%
  \ks@add{\end{tabular}}%
  \begingroup
    \tabcolsep=.6em %
    \the\ks@tab
  \endgroup
}

\RequirePackage[manualmark,draft=false]{scrlayer-scrpage}
\clearpairofpagestyles
\lohead{\ks@head}
\lofoot{\ks@foot}

\definecolor{PTBcolor}{RGB}{0,155,206}% TODO: Besser CMYK?

\input{ks-common.clo}% Titelseiten- und Layout-Definition
\input{\ks@kalType\ks@layout.clo}% Titelseiten- und Layout-Definition

\newcommand*\ks@type{%
  Typ des Ger\"ates}
\newcommand*\type[1]{%
  \begingroup
    \let\\=\relax\let\newline=\relax % nötig?
    \gdef\ks@type{#1}%
  \endgroup
}

\newcommand*\ks@serialNo{XXX,YYY}
\newcommand*\serialNoXXX[1]{%
  \xdef\ks@serialNo{\trim@spaces{\detokenize{#1}}}%
}
\newcommand*\serialNo[1]{%
  \gdef\ks@serialNo{#1}%
}

\newcommand*\ks@manufacturer{Namen der Hersteller}
\newcommand*\manufacturer[1]{%
  %%%\xdef\ks@manufacturer{\trim@spaces{\detokenize{#1}}}%
  \long\xdef\ks@manufacturer{#1}%
}

\newcommand*\ks@object{%
  Name des Ger\"ates, kurze Charakterisierung}
\newcommand\object[1]{\gdef\ks@object{#1}}

\newcommand*\ks@applicant{%
  Name des Auftraggebers, Stra\ss e, Firmensitz}
\newcommand*\applicant[1]{\long\gdef\ks@applicant{#1}}

\newcommand*\ks@refNo{YYY-ZZZZZZZZ}
\newcommand*\refNo[1]{\xdef\ks@refNo{\trim@spaces{#1}}}

\newcommand*\ks@calibMark{YYY-XXXXXXX}
\newcommand*\calibMark[1]{\xdef\ks@calibMark{\trim@spaces{#1}}}

\newcommand*\ks@referenceMark{YYY-XXXXXXX}
\newcommand*\referenceMark[1]{\xdef\ks@referenceMark{\trim@spaces{#1}}}

\newcommand*\ks@calibDate{????-??-??}
\newcommand*\calibDate[1]{\xdef\ks@calibDate{\trim@spaces{#1}}}

\newcommand*\ks@certificateDate{????-??-??}
\newcommand*\certificateDate[1]{\xdef\ks@certificateDate{\trim@spaces{#1}}}

\newcommand*\ks@byOrder{Peter Silie}
\newcommand*\byOrder[1]{\xdef\ks@byOrder{\trim@spaces{#1}}}

\newcommand*\ks@examiner{Ellen Bogen}
\newcommand*\examiner[1]{\xdef\ks@examiner{\trim@spaces{#1}}}

\newcommand*\ks@missingEntries{0}
\newcommand*\missingEntries[1]{\xdef\ks@missingEntries{\trim@spaces{#1}}}

\newcolumntype{C}{>{\centering\arraybackslash}X}

\RequirePackage{siunitx-PTB}

\newcounter{ks@colNum} 
\newcounter{ks@colMax} 

\newcommand*\ks@siTabColSet{%
  \ifnum\c@ks@colNum < 3 % nur für erste 3 Spalten 0-Exponent verwenden
    \sisetup{print-zero-exponent = true}%
  \else
    \sisetup{print-zero-exponent = false}%
  \fi
  \stepcounter{ks@colNum}%
  \ifnum\c@ks@colNum = \c@ks@colMax
    \setcounter{ks@colNum}{0}%
  \fi
}

\newcolumntype{R}{>{\ks@siTabColSet}r}% rechtsbündig mit untersch. 0-Exponent

\newcommand\resultTable[3][]{%
  %\begingroup
  \c@ks@colMax=#2 %
  \edef\@tempa{\noexpand\begin{longtable}{%
    @{\noexpand\extracolsep{\fill}}*\the\c@ks@colMax R
  }}%
  \expandafter\ks@add\expandafter{\@tempa}%
  \ks@add{\toprule}%
  \ifx\relax#1\relax\else
    \edef\@tempa{\noexpand\multicolumn{\the\c@ks@colMax}{@{}c@{}}{#1}%
      \noexpand\\\noexpand\toprule}%
    \expandafter\ks@add\expandafter{\@tempa}%
    %\showthe\ks@tab
  \fi
  \ks@add{#3}%
  \ks@add{\bottomrule\end{longtable}}%
  %\endgroup
}

\newcommand*\printFirstPage{%
  \thispagestyle{empty}%
  \AddToShipoutPictureBG*{\ks@printFirstPage}%
  \mbox{}\clearpage
  \global\let\ks@printFirstPage\@empty
  \global\let\printFirstPage\@empty
}

\newcommand*\ks@@foot{}% ???

\newcommand*\printLastPage{%
  \clearpage
  \global\let\ks@@foot=\ks@foot@add% ???
  \ks@lastPage
}

\newcommand*\printResultTable{%
  \begingroup
    \LTleft=0pt \LTright=0pt %
    \LTpre=\medskipamount
    \LTpost=\medskipamount
    \extrarowheight=.2ex %
    %\tabcolsep=0pt %
    \sisetup{%
      print-zero-exponent = false% default 
      %%%,tight-spacing = true% ???
    }%
    \par\mbox{}\vskip-\baselineskip\the\ks@tab
  \endgroup
}

\newcommand*\ks@applicant@table{%
  \begingroup
    \let\ks@tempa=\\%
    \let\\=\relax
    \ks@tab{}%
    \expandafter\@tfor\expandafter\ks@tempb\expandafter
      :\expandafter=\ks@applicant\do{%
      \expandafter\edef\expandafter\ks@tempb\expandafter{%
        \expandafter\trim@spaces\expandafter{\ks@tempb}}%
      \ifx\ks@tempb\@empty\else
        \expandafter\ks@add\expandafter{\ks@tempb\tabularnewline}%
      \fi
    }%
    \let\\=\ks@tempa
    \extrarowheight=-.1ex %
    \begin{tabular}[t]{@{}l@{}}%
      \the\ks@tab
    \end{tabular}%
  \endgroup
}

\AtBeginDocument{
  \ifdraft{
    \AddToShipoutPictureBG{%
      \AtTextCenter{%
        \makebox(0,0)[c]{\resizebox{\textwidth}{!}{%
          \rotatebox{54.7}{%
            \textnormal{\textsf{\textbf{%
              \color{lightgray}\ks@draftName}}}}}}%
      }%
    }%
  }{%
    \ifnum\number\ks@missingEntries>0 %
      \AddToShipoutPictureBG{%
        \AtTextCenter{%
          \makebox(0,0)[c]{\resizebox{\textwidth}{!}{%
            \rotatebox{54.7}{%
              \textnormal{\textsf{\textbf{%
                \color{lightgray}Fehler:~\ks@missingEntries}}}}}}%
        }%
      }%
    \fi
  }
}

\RequirePackage[
% factor=1500
% ,verbose=true
,final=true
% ,letterspace=100
,babel=true
]{microtype}

\emergencystretch=1em %
\pagestyle{scrheadings}

\renewcommand\section{%
  \scr@startsection{section}% name
  {1}% level
  {\z@}% indent
  {-3ex \@plus -.5ex \@minus -.2ex}% beforeskip (abs.!)
  {.6ex \@plus.2ex}%   afterskip
  {\ifnum \scr@compatibility>\@nameuse{scr@v@2.96}\relax
    \setlength{\parfillskip}{\z@ plus 1fil}\fi
    \raggedsection\normalfont\sectfont\nobreak\size@section}%
}
\newcommand\Section[2]{%
  \section{#1\newline\itshape\small#2}%
}
\newenvironment{english}[1][.8]{%
  \otherlanguage{english}%
  \useshorthands{"}
  \defineshorthand{"`}{\openautoquote}%
  \defineshorthand{"'}{\closeautoquote}%
  \par
  \ks@tempdima=#1\dimexpr\f@size pt % Schriftgröße skalieren; Durchschuss 130%
  \fontsize{\ks@tempdima}{1.3\ks@tempdima}\selectfont
}{%
  \par
  \endotherlanguage
}

%\widowpenalties 2 10000 0 %
\usepackage[defaultlines=2]{nowidow}
\setnowidow% keine einzelne Zeile am Seitenbeginn (keine "Hurenkinder")
% https://tex.stackexchange.com/questions/21983/how-to-avoid-page-breaks-inside-paragraphs

\endinput
%%
%% End of file `kailbrierschein.cls'.

sudo cp $(kpsewhich --var-value TEXMFSYSVAR)/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf

sudo fc-cache -fsv

mkluatexfontdb --force --verbose=-1 -vvv









