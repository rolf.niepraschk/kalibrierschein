## Kalibrierschein
Das Projekt enthält eine LaTeX-Dokumentenklasse und andere Komponenten zur 
Erzeugung eines PTB-konformen Kalibrierscheins.

Zur Erzeugung der PDF-Datei ist Folgendes zu tun:

```bash
lualatex kalibrierschein-test.tex # oder: xelatex kalibrierschein-test.tex
```

Vorausgesetzt wird eine aktuelle TeX-Distribution (getestet mit »TeX Live 
2020« unter Linux).

Es sind zwei LaTeX-Läufe nötig, um alle Referenzen (z.B. Anzahl der Seiten) 
aufzulösen. Der TeX-Compiler muss auch Zugriff auf die Grafikdateien 
`Adler.pdf`, `PTB-Logo.pdf` und `CIPM-MRA.png` haben. 

Im Repositorium ist weiterhin das LaTeX-Paket `siunitx-PTB.sty` enthalten. Es 
stellt eine Erweiterung des LaTeX-Pakets 
[»siunitx.sty«](https://www.ctan.org/tex-archive/macros/latex/contrib/siunitx) 
dar. Darin enthalten ist die automatische Anpassung an deutsche bzw. englische 
Gepflogenheiten (z.B. `,` statt `.` als Dezimaltrenner) sowie die Erweiterung 
der unterstützten Maßeinheiten, auch wenn sie nicht SI-konform sein sollten 
(derzeit nur für die Belange des Vakuum-Labors). 

Die Datei `kalibrierschein-test.tex` ist zuvor aus einer Template-Datei 
entstanden, indem die in dieser enthaltenen Variablen durch tatsächliche Werte 
ersetzt wurden. Diese Werte werden durch Analyse einer DCC-XML-Datei ermittelt. 
Das Python-Script `sample_cal_creation.py` zeigt das Prinzip:

```bash
./sample_cal_creation.py templates/kalibrierschein-test.tex.tpl < kalibrierschein-test.xml > kalibrierschein-test.tex
```

Der einzige notwendige Parameter ist die Template-Datei. Die XML-Struktur wird 
auf `stdin` erwartet und die resultierende LaTeX-Datei wird nach `stdout` 
geschrieben (Unix-Filter). 

Derzeit werden nur Informationen, die auf der Titelseite des Kalibrierscheins 
verwendet werden, extrahiert (Erweiterung ist geplant). Das Python3-Script 
`sample_cal_creation.py` verwendet die verbreiteten Python-Bibliotheken 
[»python-lxml«](https://lxml.de/) (XML-Verarbeitung) und 
[»python-Jinja2«](http://jinja.pocoo.org/) (Template-Verarbeitung) jeweils in 
der Python3-Version. Der hier beschriebene Ablauf kann auch per
```
make
```
in Gang gesetzt werden. Er wurde unter Linux (openSUSE) getestet.

Den Ersetzungsprozess, der von `sample_cal_creation.py` vollzogen wird, kann 
auch leicht in eine Serveranwendung ausgelagert werden (beispielsweise 
basierend auf der Python-Bibliothek `Flask`).

--
R.N.
