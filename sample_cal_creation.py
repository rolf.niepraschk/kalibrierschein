#!/usr/bin/python3
import sys, re
from lxml import etree
from jinja2_latex import texenv

try:
    tpl = open(sys.argv[1], 'r')
except (IndexError, FileNotFoundError):
    sys.stderr.write('command parameter "template file" is missing -- exit'+ '\n')
    sys.exit()

template = texenv.from_string(tpl.read())
doc = etree.parse(sys.stdin)
root = doc.getroot()
ns = root.nsmap

def getElementList(_root, _path, _ns):
    try:
        ret = _root.findall(_path, namespaces=_ns)
    except:
        ret = []
    return ret
def getText(_root, _path, _ns):
    try:
        ret = _root.find(_path, _ns).text.strip()
    except:
        ret = ''
    return ret

# Extract data from DCC-XML to the list "cal_data" 
    
cal_data = {}

cal_data['lang'] = lang = getText(root, \
  'dcc:administrativeData/dcc:coreData/dcc:usedLangCodeISO639_1[1]', ns) # ???
cal_data['lang'] = lang = 'en' # ???  

cal_data['Certificate'] = getText(root, \
  'dcc:administrativeData/dcc:coreData/dcc:uniqueIdentifier', ns) # ???
cal_data['MeasurementDate'] = getText(root, \
  'dcc:administrativeData/dcc:coreData/dcc:beginPerformanceDate', ns) 
cal_data['calibration_type'] = 'KK' # ???

cal_data['Type'] = 'Head: 690A01TRA, Contr: 670 BD81 (XML?)'
cal_data['ReferenceNo'] = '7.5-1.4-19-3-5 (XML?)'
# In XML fehlt: "\byOrder{Dr.\,Matthias Bernien}"
cal_data['CertificateDate'] = '2019-03-15 (XML?)'

el = getElementList(root, 'dcc:administrativeData/dcc:items/dcc:item', ns)
for e in el:
    cal_data['ObjectOfCalibration'] = getText(e, \
      'dcc:name/dcc:content[@lang="'+ lang + '"]', ns)
    cal_data['Producer'] = getText(e, \
      'dcc:manufacturer/dcc:name', ns)
    el1 = getElementList(e, 'dcc:identifications/dcc:identification', ns)
    for e1 in el1:
        cal_data['Serial'] = getText(e1, 'dcc:value', ns)
        break # nur erstes "identification" (!?)
    break # nur erstes "item" (!?)
    
cal_data['CustomerName'] = getText(root, \
  'dcc:administrativeData/dcc:customer/dcc:name', ns)
cal_data['CustomerStreet'] = getText(root, \
  'dcc:administrativeData/dcc:customer/dcc:location/dcc:street', ns) 
cal_data['CustomerZipcode'] = getText(root, \
  'dcc:administrativeData/dcc:customer/dcc:location/dcc:postCode', ns)
cal_data['CustomerTown'] = getText(root, \
  'dcc:administrativeData/dcc:customer/dcc:location/dcc:city', ns)
cal_data['CustomerLand'] = getText(root, \
  'dcc:administrativeData/dcc:customer/dcc:location/dcc:countryCode', ns)
# TODO: 'countryCode' --> Ländername (en/de) 
cal_data['Examiner'] = getText(root, \
'dcc:administrativeData/dcc:respPersons/\
dcc:respPerson[@id="Technician"]/dcc:person', ns)

sys.stderr.write('#######################################################\n')
sys.stderr.write (str(cal_data) + '\n')  


# render the template with "cal_data" and write the result to stdout  
print(template.render(cal_data))


